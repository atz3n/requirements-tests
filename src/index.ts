
export class StartButton {
    private color = "";

    constructor(color = "green") {
        this.color = color;
    }

    public getColor(): string {
        return this.color;
    }
}