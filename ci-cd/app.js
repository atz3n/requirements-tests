const testResults = require("../testResults.json");
const fs = require("fs");


const requirementStates = {};

testResults.testResults.forEach(assertionResultsObject => {
    assertionResultsObject.assertionResults.forEach(result => {
        
        if(result.title.startsWith("REQ-")) {
            let status = "failed";
            if(result.status === "passed") {
                status = "passed";
            }

            const endOfRequirementString = result.title.indexOf(":");
            const requirementId = result.title.substring(4, endOfRequirementString);
            requirementStates[requirementId] = status;
        }
    });
});


fs.mkdirSync("tmp");
fs.writeFileSync("tmp/requirements.json", JSON.stringify(requirementStates));