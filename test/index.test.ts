import { StartButton } from "../src";


describe("Start Button test", () => {

    test("REQ-1: color is blue success", async () => {
        const button = new StartButton("blue");
        expect(button.getColor()).toBe("blue");
    });


    test("REQ-2: color is green success", async () => {
        const button = new StartButton("green");
        expect(button.getColor()).toBe("green");
    });


    test("REQ-3: color is yellow success", async () => {
        const button = new StartButton("yellow");
        expect(button.getColor()).toBe("yellow");
    });


    test("REQ-4: color is pink success", async () => {
        const button = new StartButton("pink");
        expect(button.getColor()).toBe("pink");
    });

});